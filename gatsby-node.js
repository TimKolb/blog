/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */
const path = require('path');
const { createFilePath } = require('gatsby-source-filesystem');

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;
  // We only want to operate on `Mdx` nodes. If we had content from a
  // remote CMS we could also check to see if the parent node was a
  // `File` node here
  if (node.internal.type === 'Mdx') {
    const value = createFilePath({ node, getNode });
    const { fileAbsolutePath } = node;
    const prefix = fileAbsolutePath.includes('/content/blog') ? '/blog' : '';

    createNodeField({
      // Name of the field you are adding
      name: 'slug',
      // Individual MDX node
      node,
      // Generated value based on filepath with "blog" prefix
      value: `${prefix}${value}`,
    });
  }
};

exports.createPages = ({ graphql, actions }) => {
  // Destructure the createPage function from the actions object
  const { createPage } = actions;
  return new Promise((resolve, reject) => {
    resolve(
      graphql(
        `
          {
            allMdx(sort: { fields: frontmatter___date, order: DESC }) {
              edges {
                node {
                  id
                  frontmatter {
                    title
                  }
                  fields {
                    slug
                  }
                }
              }
            }
          }
        `
      ).then(result => {
        if (result.errors) {
          console.error(result.errors);
          reject(result.errors);
        }
        result.data.allMdx.edges.forEach(({ node }, index, all) => {
          function filterBlogPosts(node) {
            return node.fields.slug.startsWith('/blog/') ? node : null;
          }

          const previous =
            index === all.length - 1
              ? null
              : filterBlogPosts(all[index + 1].node);
          const next =
            index === 0 ? null : filterBlogPosts(all[index - 1].node);
          createPage({
            // This is the slug we created before
            path: node.fields.slug,
            // This component will wrap our MDX content
            component: path.resolve(`./src/templates/BlogPostTemplate.tsx`),
            // We can use the values in this context in
            // our page layout component
            context: {
              id: node.id,
              previous,
              next,
            },
          });
        });
      })
    );
  });
};
