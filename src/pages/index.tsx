import * as React from 'react';
import { Intro } from '../components/Intro';
import { Seo } from '../components/Seo';
import { Layout } from '../layout';

export type IndexPageProps = {};
export const IndexPage: React.FC<IndexPageProps> = () => {
  return (
    <>
      <Layout renderPage={({ children }) => children}>
        <Seo
          title="Home"
          description="Hi, I'm Tim Kolberger. I blog about [React, GraphQL, DevOps, ...more]"
        />
        <Intro />
      </Layout>
    </>
  );
};

export default IndexPage;
