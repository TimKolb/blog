import * as React from 'react';
import { graphql } from 'gatsby';
import { Layout } from '../layout';
import { Seo } from '../components/Seo';
import { IconRepeat } from '../components/IconRepeat';
import Img, { FluidObject } from 'gatsby-image';
import {
  Box,
  Flex,
  Heading,
  HStack,
  useColorModeValue,
  VStack,
  Text,
} from '@chakra-ui/react';
import { BreakoutLink } from '../components/BreakoutLink';
import { AutoGrid } from '../components/AutoGrid';

type BlogIndexProps = {
  data: {
    site: {
      siteMetadata: {
        title: string;
        description: string;
      };
    };
    allMdx: {
      edges: {
        node: {
          id: string;
          fields: {
            slug: string;
          };
          timeToRead: string;
          frontmatter: {
            date: string;
            title: string;
            spoiler: string;
            hero?: {
              childImageSharp: {
                fluid: FluidObject;
              };
            };
          };
        };
      }[];
    };
  };
};

const styles = {
  boxShadow: 'lg',
  '.article-image': {
    transform: 'translateY(-2.5%) scale(1.05)',
    transformOrigin: 'bottom bottom',
  },
};

const hoverStyles = {
  sx: {
    transitionProperty: 'box-shadow',
    transitionDuration: '150ms',
    transitionTimingFunction: 'ease-in',
    boxShadow: 'md',
    '.article-image': {
      transitionProperty: 'transform',
      transitionDuration: '150ms',
      transitionTimingFunction: 'ease-in',
    },
    _hover: styles,
    _focusWithin: styles,
  },
};

function BlogIndexTemplate(props: BlogIndexProps) {
  const siteTitle = props.data.site.siteMetadata.title;
  const posts = props.data.allMdx.edges;
  const cardBg = useColorModeValue('gray.50', 'whiteAlpha.200');
  const secondaryText = useColorModeValue('gray.600', 'gray.400');

  return (
    <Layout title={siteTitle}>
      <Seo title="Blog" />
      <VStack as="section" spacing="8" align="stretch">
        <Heading as="h1" fontSize="4xl">
          Blog
        </Heading>
        <AutoGrid>
          {posts.map(
            ({
              node: {
                id,
                fields: { slug },
                frontmatter: { title, hero, spoiler, date },
                timeToRead,
              },
            }) => {
              return (
                <Flex
                  key={id}
                  position="relative"
                  overflow="hidden"
                  direction="column"
                  bg={cardBg}
                  borderRadius="lg"
                  {...hoverStyles}
                >
                  <VStack
                    as="article"
                    align="stretch"
                    spacing="4"
                    flex="1 0 auto"
                  >
                    {hero && (
                      <Img
                        fluid={hero.childImageSharp.fluid}
                        style={{ height: '8rem' }}
                        className="article-image"
                      />
                    )}
                    <VStack
                      as="section"
                      align="stretch"
                      spacing="4"
                      p="4"
                      pt="0"
                      flex="1 0 auto"
                    >
                      <header>
                        <BreakoutLink to={slug} rel="bookmark">
                          <Heading as="h3" fontSize="lg">
                            {title}
                          </Heading>
                        </BreakoutLink>
                      </header>

                      <Text
                        flex="1 0 auto"
                        dangerouslySetInnerHTML={{
                          __html: spoiler,
                        }}
                      />

                      <HStack
                        as="footer"
                        fontSize="xs"
                        color={secondaryText}
                        justify="flex-end"
                      >
                        <IconRepeat
                          icon="☕️"
                          label="coffee"
                          amount={Math.max(
                            1,
                            Math.round(parseInt(timeToRead) / 5)
                          )}
                        />
                        <span>{timeToRead} min read</span>
                        <Box as="time">
                          {new Date(date).toLocaleDateString()}
                        </Box>
                      </HStack>
                    </VStack>
                  </VStack>
                </Flex>
              );
            }
          )}
        </AutoGrid>
      </VStack>
    </Layout>
  );
}

export default BlogIndexTemplate;

export const pageQuery = graphql`
  {
    site {
      siteMetadata {
        title
        description
      }
    }
    allMdx(
      filter: {
        fileAbsolutePath: { regex: "content/blog/i" }
        frontmatter: { isDraft: { ne: true }, title: { ne: "" } }
      }
      sort: { fields: [frontmatter___date], order: DESC }
    ) {
      edges {
        node {
          id
          timeToRead
          fields {
            slug
          }
          frontmatter {
            date
            title
            spoiler
            hero {
              childImageSharp {
                fluid(maxWidth: 700) {
                  src
                  srcSet
                  sizes
                  aspectRatio
                }
              }
            }
          }
        }
      }
    }
  }
`;
