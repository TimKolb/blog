import { css } from '@emotion/core';
import windowButtons from '../images/window-buttons.svg';

export const codeBlockCSS = css`
  .gatsby-highlight {
    font-size: 1rem;
    margin: 0 -1.0875rem;
    box-shadow: 0 0 1em 0 rgba(0, 0, 0, 0.5);

    &::before {
      content: '';
      display: block;
      height: 0.857rem;
      background: #011627 url(${windowButtons}) no-repeat 0.5em center;
      margin-bottom: -0.5em;
      margin-top: 0.5em;
      padding: 1em 0.5rem 0;
    }

    @media screen and (min-width: 48.425rem) {
      border-radius: 0.2rem;

      code[class*='language-'],
      pre[class*='language-'] {
        border-bottom-left-radius: 0.2rem;
        border-bottom-right-radius: 0.2rem;
      }

      &::before {
        border-top-left-radius: 0.2rem;
        border-top-right-radius: 0.2rem;
      }
    }
  }
`;
