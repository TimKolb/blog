---
title: 'Frontend - Tutorial: Road to production'
date: 2019-05-03T10:00:00.000Z
spoiler: From zero to production - a setup guide for a new React project
hero: ./road-to-production-frontend-hero.jpg
heroCredit: Photo by Mikito Tateisi on Unsplash
---

# Tutorial: Road to production - Frontend

There are many tutorials and endless resources out there to get started with React and build your first web application.
But when you are done with the initial hassle with UI and business logic, you are stuck.
You want to get your application live.
You want to provide a service for other people and make the world a better place.

Follow me through a fully fledged project setup.
I work on macOS but most of the steps should be easy to be ported to another platform.

In this tutorial we are going to cover the following topics:

- **development** setup for a React frontend application
- **production** setup with docker for the frontend application

Let's get started.

## Create a project directory

We need a base project directory in which all project related files are organized.

1. Create a new directory
1. Change into it
1. Initialize a new git repository
1. Create a simple _README.md_ file
1. Add and commit the _README.md_ as the initial commit

```bash
mkdir my-project
cd my-project
git init
echo "# My Project" > README.md
git add .
git commit -m "Initial commit"
```

If git on the command line is new to you, have a look at this goodie: [GitLab git cheat sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf)

## Initialize the frontend project

For the frontend project we will use [React](https://reactjs.org/).
I you prefer another frontend framework like [Angular](https://angular.io/) you can skip this frontend setup and roll your own.

Every modern age frontend framework depends on a build process, which transforms the written code to executable JavaScript, HTML and CSS.
This build process is performed by a module bundler like [webpack](https://webpack.js.org/).
It is a time and nerve consuming part of web development which just a few developer like to deal with.

Most of the frontend frameworks come with a preconfigured development and production build setup.
React provides a development setup called _create-react-app_, Angular has its _angular-cli_, etc.

So let us start with installing a _create-react-app_ project with [TypeScript](https://www.typescriptlang.org/).

```bash
npx create-react-app frontend --typescript --use-npm
git add frontend
git commit -m "Add frontend project"
```

The project directory should now look like this:

```bash
> tree -I "node_modules|.git" -a
.
├── README.md
└── frontend
    ├── .gitignore
    ├── README.md
    ├── craco.config.js
    ├── package-lock.json
    ├── package.json
    ├── public
    │   ├── favicon.ico
    │   ├── index.html
    │   └── manifest.json
    ├── src
    │   ├── App.css
    │   ├── App.test.tsx
    │   ├── App.tsx
    │   ├── index.css
    │   ├── index.tsx
    │   ├── logo.svg
    │   ├── react-app-env.d.ts
    │   └── serviceWorker.ts
    └── tsconfig.json

3 directories, 18 files
```

## Add Ant Design with craco

_Where we at?_ - We have a working frontend setup and we could start implementing a bunch of components and reinvent the wheel again.
Let's use a design system provided as component library.

[Ant Design - The world's second most popular React UI framework](https://ant.design/docs/react/introduce)

I do not have any clue why they call themselves the _second most popular React UI framework_.
The Ant Design component library has a feature rich set of components and looks good from the start.

_create-react-app_ does not provide any API to alter the webpack configuration.
This restriction protects the users and maintainers of the _create-react-app_ project.

But well, we think we can handle it 😎
And so are the maintainers of some projects which provide the functionality of altering the webpack configuration of _create-react-app_.
My preferred _create-react-app_-override-project is [craco](https://github.com/sharegate/craco).

```bash
cd frontend
npm install @craco/craco craco-antd antd
```

Create the craco configuration file and add the AntDesign plugin.

```js
// frontend/craco.config.js
const CracoAntDesignPlugin = require('craco-antd');

module.exports = {
  plugins: [{ plugin: CracoAntDesignPlugin }],
};
```

Replace the package.json scripts with the craco ones.

```json
// frontend/package.json
{
  // ...
  "scripts": {
    "start": "craco start",
    "build": "craco build",
    "test": "craco test"
  }
}
```

```bash
git add .
git commit -m "Add craco and ant design"
```

## Try our frontend setup

```bash
npm start
```

![create-react-app screen](./create-react-app-screen.jpg)

Now we are ready to develop 🙌
