import * as React from 'react';
import { Layout } from './Layout';
import { Heading } from '@chakra-ui/react';
import { Seo } from '../components/Seo';

export type PageLayoutProps = {
  pageContext: { frontmatter: { title?: string } };
};
export const PageLayout: React.FC<PageLayoutProps> = ({
  children,
  pageContext: {
    frontmatter: { title },
  },
}) => {
  const heading = title ? (
    <Heading as="h1" size="xl">
      {title}
    </Heading>
  ) : null;

  return (
    <Layout>
      <Seo title={title} />
      {heading}
      {children}
    </Layout>
  );
};

export default PageLayout;
