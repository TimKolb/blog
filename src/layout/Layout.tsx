import React, { ReactNode } from 'react';
import { graphql, StaticQuery } from 'gatsby';
import { Box, BoxProps, Flex } from '@chakra-ui/react';
import { Header } from '../components/Header';
import { Footer } from '../components/Footer';
import { PageTransition } from '../components/PageTransition';

const layoutQuery = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`;

type LayoutQueryData = {
  site: {
    siteMetadata: {
      title: string;
    };
  };
};

export type LayoutProps = BoxProps & {
  children: ReactNode;
  renderPage?: (props: { children: ReactNode }) => ReactNode;
};

const defaultRenderPage: LayoutProps['renderPage'] = (props) => (
  <Page p="4" {...props} />
);

export const Layout = ({
  renderPage = defaultRenderPage,
  children,
}: LayoutProps) => (
  <StaticQuery<LayoutQueryData>
    query={layoutQuery}
    render={(data) => (
      <Flex direction="column" minH="100vh" align="stretch" position="relative">
        <Header siteTitle={data.site.siteMetadata.title} />
        {renderPage({ children })}
        <Footer />
      </Flex>
    )}
  />
);

export const Page = ({ children, ...props }: BoxProps) => (
  <PageTransition>
    <Box
      as="main"
      w="full"
      maxW="75rem"
      py="4"
      px={['2', '4']}
      flex="1 0 auto"
      mt="4"
      mx="auto"
      {...props}
    >
      {children}
    </Box>
  </PageTransition>
);
