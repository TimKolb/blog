export const mdx = {
  h1: {
    mt: '8',
    mb: '2',
    lineHeight: 'shorter',
    fontWeight: 'bold',
    fontSize: '3xl',
    letterSpacing: 'tight',
  },
  h2: {
    mt: '8',
    mb: '2',
    lineHeight: 'short',
    fontWeight: 'semibold',
    fontSize: '2xl',
    letterSpacing: 'tight',
    '& + h3': {
      mt: '6',
    },
  },
  h3: {
    mt: '8',
    mb: '2',
    lineHeight: 'shorter',
    fontWeight: 'semibold',
    fontSize: 'xl',
    letterSpacing: 'tight',
  },
  h4: {
    mt: '6',
    lineHeight: 'short',
    fontWeight: 'semibold',
    fontSize: 'lg',
  },
  a: {
    fontWeight: 'semibold',
    transition: 'color 0.15s',
    transitionTimingFunction: 'ease-out',
  },
  p: {
    mt: '1.25rem',
    lineHeight: 1.7,
    'blockquote &': {
      mt: 0,
    },
  },
  hr: {
    my: '4rem',
  },
  ul: {
    mt: '1.5rem',
    ml: '1.25rem',
    'blockquote &': { mt: 0 },
    '& > * + *': {
      mt: '0.25rem',
    },
  },
  code: {
    rounded: 'sm',
    px: '1',
    fontSize: '0.875em',
    py: '2px',
    whiteSpace: 'nowrap',
    lineHeight: 'normal',
  },
};
