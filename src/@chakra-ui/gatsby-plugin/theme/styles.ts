import { mode } from '@chakra-ui/theme-tools';
import { ThemeOverride } from '@chakra-ui/react/dist/types/extend-theme';
import type { MyTheme } from './index';

export const styles: ThemeOverride['styles'] = {
  global: (props: MyTheme) => ({
    body: {
      bg: mode('gray.100', 'gray.700')(props),
    },
    a: {
      color: mode('green.600', 'green.400')(props),
      _hover: {
        color: mode('green.700', 'green.300')(props),
      },
    },
    'a.anchor': {
      display: 'inline-block',
      mr: '1',
      svg: {
        fill: mode('gray.400', 'gray.600')(props),
      },
    },
  }),
};
