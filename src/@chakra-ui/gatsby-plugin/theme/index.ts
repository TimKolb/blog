import { extendTheme } from '@chakra-ui/react';
import { fonts } from './fonts';
import { colors } from './colors';
import { styles } from './styles';
import { mdx } from './mdx';

const theme = extendTheme({
  colors,
  fonts,
  mdx,
  styles,
  config: {
    initialColorMode: 'dark',
    useSystemColorMode: false,
  },
});

export type MyTheme = typeof theme;

export default theme;
