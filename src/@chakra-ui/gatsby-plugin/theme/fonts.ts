const fallbackFontFamily =
  "-apple-system, BlinkMacSystemFont, 'Segoe UI', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';";

export const fonts = {
  body: `Catamaran, ${fallbackFontFamily}`,
  heading: `'Mukta Vaani', ${fallbackFontFamily}`,
};
