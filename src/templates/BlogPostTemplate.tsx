import * as React from 'react';
import { graphql } from 'gatsby';
import { GatsbyImageFluidProps } from 'gatsby-image';
import { chakra, Container, HStack, Icon } from '@chakra-ui/react';
import { MDXRenderer } from 'gatsby-plugin-mdx';
import { Seo } from '../components/Seo';
import { ScrollToc } from '../components/ScrollToc';
import { HeroImage } from '../components/HeroImage';
import { Link, LinkProps } from '../components/LinkChakraGatsby';
import '../styles/nightOwl.css';
import { Layout, Page } from '../layout';
import { FiArrowLeft, FiArrowRight } from 'react-icons/fi';

const PostLink = (props: LinkProps) => (
  <Link
    d="inline-flex"
    alignItems="center"
    flex="0 1 50%"
    p="4"
    borderRadius="md"
    border="1px"
    borderColor="green.500"
    _hover={{
      textDecoration: 'none',
    }}
    {...props}
  />
);

export type TocEntry = {
  url?: string;
  title?: string;
  items?: TocEntry[];
};

export type Toc = {
  items: TocEntry[];
};

type SiblingPage = {
  id: string;
  frontmatter: { title: string };
  fields: {
    slug: string;
  };
};

type BlogPostProps = {
  pageContext: {
    isCreatedByStatefulCreatePages: boolean;
    id: string;
    previous: SiblingPage | null;
    next: SiblingPage | null;
  };
  data: {
    mdx: {
      tableOfContents: Toc;
      body: string;
      frontmatter: {
        title: string;
        spoiler?: string;
        date?: string;
        hero?: {
          childImageSharp: {
            fluid: GatsbyImageFluidProps['fluid'];
          };
        };
        heroCredit?: string;
      };
    };
  };
};

const BlogPostTemplate: React.FC<BlogPostProps> = ({
  data: {
    mdx: {
      body,
      tableOfContents,
      frontmatter: { title, spoiler, hero, heroCredit },
    },
  },
  pageContext: { next, previous },
}) => {
  const containerWidth = '60ch';
  const tocWidth = '25ch';

  return (
    <Layout
      renderPage={(props) => (
        <Page w="full" maxW="full" px="0" py="0" mt="0" {...props} />
      )}
    >
      <Seo title={title} description={spoiler} />
      {hero && <HeroImage {...hero.childImageSharp} credit={heroCredit} />}
      <Container
        maxW={containerWidth}
        sx={{
          '.toc': {
            mb: '4',
          },
          [`@media screen and (min-width: calc(${containerWidth} + ${tocWidth} + ${tocWidth} + 14ch))`]: {
            '.toc': {
              position: 'sticky',
              top: '2',
              left: '0',
              width: tocWidth,
              transform: `translateX(-${tocWidth})`,
              px: '4',
              height: 0,
              mb: '0',
              '> *': {
                my: '2',
              },
            },
          },
        }}
      >
        <ScrollToc tableOfContents={tableOfContents} className="toc" />
        <article>
          <MDXRenderer>{body}</MDXRenderer>
          {(previous || next) && (
            <chakra.footer mt="8">
              <HStack as="nav" align="stretch" spacing="8">
                {previous ? (
                  <PostLink to={previous.fields.slug}>
                    <Icon as={FiArrowLeft} mr="2" />
                    {previous.frontmatter.title}
                  </PostLink>
                ) : (
                  <chakra.span aria-hidden mr="auto" />
                )}
                {next && (
                  <PostLink
                    to={next.fields.slug}
                    justifyContent="flex-end"
                    ml="auto"
                  >
                    {next.frontmatter.title}
                    <Icon as={FiArrowRight} />
                  </PostLink>
                )}
              </HStack>
            </chakra.footer>
          )}
        </article>
      </Container>
    </Layout>
  );
};

export default BlogPostTemplate;

export const pageQuery = graphql`
  query BlogPostQuery($id: String) {
    mdx(id: { eq: $id }) {
      id
      frontmatter {
        title
        spoiler
        date
        hero {
          childImageSharp {
            fluid(maxWidth: 2048) {
              src
              srcSet
              sizes
              aspectRatio
            }
          }
        }
        heroCredit
      }
      tableOfContents
      body
    }
  }
`;
