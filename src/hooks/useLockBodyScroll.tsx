import { useLayoutEffect } from 'react';

export function useLockBodyScroll({ lock = true }: { lock?: boolean } = {}) {
  useLayoutEffect(() => {
    if (!lock) {
      return;
    }
    const originalStyle = window.getComputedStyle(document.body).overflow;
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = originalStyle;
    };
  }, [lock]);
}
