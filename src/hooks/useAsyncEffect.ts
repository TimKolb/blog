import { DependencyList, useEffect } from 'react';

export function useAsyncEffect(
  effect: () => Promise<void>,
  cleanUpFn?: () => void,
  inputs?: DependencyList
) {
  useEffect(function asyncEffectWrapper() {
    effect();
    return cleanUpFn;
  }, inputs);
}
