import { useLayoutEffect, useState } from 'react';

export function useRefreshOnClient() {
  const [isClient, setIsClient] = useState(false);

  useLayoutEffect(() => {
    setIsClient(true);
  });

  return {
    isClient,
    clientClassName: isClient ? 'is-client' : 'is-server',
  };
}
