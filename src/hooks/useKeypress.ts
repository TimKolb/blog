import { useEffect, useMemo, useState } from 'react';

export function useKeyPress({
  targetKey,
  onDown,
  onUp,
  withCTRL = false,
  withSHIFT = false,
  withALT = false,
}: {
  targetKey?: string;
  onDown?: (event: KeyboardEvent) => any;
  onUp?: (event: KeyboardEvent) => any;
  withCTRL?: boolean;
  withSHIFT?: boolean;
  withALT?: boolean;
}) {
  const [keyPressed, setKeyPressed] = useState(false);

  function createKeyEventHandler({
    callback,
    keyState,
  }: {
    callback?: (event: KeyboardEvent) => any;
    keyState: boolean;
  }) {
    return function handler(event: KeyboardEvent) {
      const { key, altKey, ctrlKey, shiftKey } = event;
      if (
        (withCTRL && !ctrlKey) ||
        (withSHIFT && !shiftKey) ||
        (withALT && !altKey)
      ) {
        return;
      }

      if (!targetKey || key === targetKey) {
        setKeyPressed(keyState);
        callback && callback(event);
      }
    };
  }

  const downHandler = useMemo(
    () => createKeyEventHandler({ callback: onDown, keyState: true }),
    [onDown]
  );

  const upHandler = useMemo(
    () => createKeyEventHandler({ callback: onUp, keyState: false }),
    [onUp]
  );

  useEffect(() => {
    window.addEventListener('keydown', downHandler);
    window.addEventListener('keyup', upHandler);
    return () => {
      window.removeEventListener('keydown', downHandler);
      window.removeEventListener('keyup', upHandler);
    };
  }, [downHandler, upHandler]);

  return keyPressed;
}
