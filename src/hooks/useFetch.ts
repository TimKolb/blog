import axios, { AxiosRequestConfig, CancelTokenSource } from 'axios';
import { useAsyncEffect } from './useAsyncEffect';
import { useMemo, useReducer, useRef } from 'react';

type FetchReducerState = {
  loading: boolean;
  data: any;
  error: Error | null;
};

type FetchReducerAction = {
  type: 'START' | 'ERROR' | 'SUCCESS';
  payload?: Partial<FetchReducerState>;
};

export const initialFetchState = {
  loading: false,
  data: null,
  error: null,
};

export function fetchReducer(
  state: FetchReducerState,
  action: FetchReducerAction
) {
  switch (action.type) {
    case 'START':
      return {
        ...state,
        error: null,
        loading: false,
      };
    case 'SUCCESS':
      return {
        ...state,
        ...action.payload,
        error: null,
      };
    case 'ERROR':
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
}

const CancelToken = axios.CancelToken;
export function useFetch(url: string, config?: AxiosRequestConfig) {
  const [fetchState, dispatch] = useReducer(fetchReducer, initialFetchState);

  const requestSource = useRef<CancelTokenSource | null>(null);
  const cancel = () => {
    if (requestSource.current) {
      requestSource.current.cancel();
    }
  };

  useAsyncEffect(
    async () => {
      try {
        requestSource.current = CancelToken.source();
        dispatch({ type: 'START' });
        const result = await axios(url, {
          ...config,
          cancelToken: requestSource.current.token,
        });
        dispatch({ type: 'SUCCESS', payload: { data: result.data } });
      } catch (error) {
        dispatch({ type: 'ERROR', payload: { error } });
      }
    },
    cancel,
    [url, config]
  );

  return useMemo(
    () => ({
      ...fetchState,
      cancel,
    }),
    [fetchState, requestSource]
  );
}
