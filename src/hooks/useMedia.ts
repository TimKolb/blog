import { useEffect, useState } from 'react';

export function useMedia<T = any>(
  queries: string[],
  values: T[],
  defaultValue: T
) {
  const match = () => {
    if (typeof matchMedia === 'undefined') {
      return defaultValue;
    }
    const query = queries.findIndex(q => matchMedia(q).matches);
    return values[query] || defaultValue;
  };

  const [value, set] = useState(match);

  useEffect(() => {
    if (typeof window === 'undefined') {
      return;
    }
    const handler = () => set(match);
    window.addEventListener('resize', handler);
    return () => window.removeEventListener('resize', handler);
  }, []);

  return value;
}
