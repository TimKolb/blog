import { useState } from 'react';

export function useToggle(initialState = false): [boolean, () => void] {
  const [open, setOpen] = useState(initialState);
  const onToggle = () => {
    setOpen(state => !state);
  };
  return [open, onToggle];
}
