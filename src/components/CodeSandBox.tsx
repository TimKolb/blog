import * as React from 'react';
import { Iframe } from './Iframe';
import { useMedia } from '../hooks/useMedia';
import { chakra } from '@chakra-ui/react';

export const CodeSandBox: React.FC<{ box: string; title: string }> = ({
  box,
  title,
}) => {
  const isBigScreen = useMedia(
    // Media queries
    ['(min-width: 48rem)'],
    // Column counts (relates to above media queries by array index)
    [true],
    // Default column count
    false
  );
  const explicitTitle = `CodeSandBox | ${title}`;

  if (!isBigScreen) {
    return (
      <chakra.a
        href={`https://codesandbox.io/s/${box}?fontsize=14`}
        target="_blank"
        rel="noopener noreferrer"
        display="block"
        textAlign="center"
      >
        <img
          alt={explicitTitle}
          src="https://codesandbox.io/static/img/play-codesandbox.svg"
        />
      </chakra.a>
    );
  }
  return (
    <Iframe
      src={`https://codesandbox.io/embed/${box}?fontsize=14`}
      title={explicitTitle}
      sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"
    />
  );
};
