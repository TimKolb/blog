import * as React from 'react';
import { HTMLMotionProps, isValidMotionProp, motion } from 'framer-motion';
import { chakra, ChakraProps, forwardRef } from '@chakra-ui/react';

const MotionBox = motion.custom(
  forwardRef((props, ref) => {
    const chakraProps = Object.fromEntries(
      // do not pass framer props to DOM element
      Object.entries(props).filter(([key]) => !isValidMotionProp(key))
    );
    return <chakra.div ref={ref} {...chakraProps} />;
  })
);

export const PageTransition = (props: ChakraProps & HTMLMotionProps<'div'>) => (
  <MotionBox
    initial={{ y: -16, opacity: 0 }}
    animate={{ y: 0, opacity: 1 }}
    transition={{ staggerChildren: 0.07, delayChildren: 0.2 }}
    flex="1 0 auto"
    d="flex"
    flexDirection="column"
    {...props}
  />
);
