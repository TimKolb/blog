import * as React from 'react';
import { FC, useEffect, useRef, useState } from 'react';
import { Canvas, extend, useFrame, useThree } from 'react-three-fiber';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

extend({ OrbitControls });

const Galaxy: FC = () => {
  const [model, setModel] = useState<GLTF>();
  useEffect(() => {
    new GLTFLoader().load('/scene.gltf', setModel);
  }, []);

  return model ? <primitive object={model.scene} /> : null;
};

const Controls: FC = () => {
  const orbitRef = useRef<{ update: Function }>();
  const { gl, camera } = useThree();

  useFrame(() => {
    orbitRef.current?.update();
  });

  return (
    <orbitControls
      ref={orbitRef}
      args={[camera, gl.domElement]}
      target={[110, 110 - 50, -110]}
      maxDistance={500}
      minDistance={100}
      autoRotateSpeed={-0.5}
      autoRotate={true}
      enableDamping={true}
      enableKeys={true}
    />
  );
};

export const EyeCatcher: FC = () => {
  if (typeof window !== 'object') {
    return null;
  }

  return (
    <Canvas>
      <Controls />
      <Galaxy />
    </Canvas>
  );
};
