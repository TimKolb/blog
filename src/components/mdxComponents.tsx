import * as React from 'react';
import { CodeSandBox } from './CodeSandBox';
import { Alert, chakra, Kbd, Table, Td, Th } from '@chakra-ui/react';
import { Link } from './LinkChakraGatsby';
import { LiveEditor, LiveError, LivePreview, LiveProvider } from 'react-live';
import nightOwlTheme from 'prism-react-renderer/themes/nightOwl';

export const components: Record<string, React.FC<any>> = {
  h1: (props) => <chakra.h1 apply="mdx.h1" {...props} />,
  h2: (props) => <chakra.h2 apply="mdx.h2" {...props} />,
  h3: (props) => <chakra.h3 apply="mdx.h3" {...props} />,
  h4: (props) => <chakra.h4 apply="mdx.h4" {...props} />,
  h5: (props) => <chakra.h5 apply="mdx.h5" {...props} />,
  h6: (props) => <chakra.h6 apply="mdx.h6" {...props} />,
  hr: (props) => <chakra.hr apply="mdx.hr" {...props} />,
  strong: (props) => <chakra.strong fontWeight="semibold" {...props} />,
  inlineCode: (props: any) => <chakra.code apply="mdx.code" {...props} />,
  pre: (props) => {
    const {
      children: {
        props: { children: grandChildren },
      },
      className,
    } = props;

    if (className !== 'language-react-live') {
      return <chakra.pre my="2" borderRadius="md" {...props} />;
    }

    return (
      <LiveProvider code={grandChildren} theme={nightOwlTheme}>
        <chakra.div bg={nightOwlTheme.plain.backgroundColor}>
          <LiveEditor />
          <LiveError />
          <LivePreview
            style={{
              borderTop: `1px solid #fafafa`,
              backgroundColor: 'inherit',
            }}
          />
        </chakra.div>
      </LiveProvider>
    );
  },

  kbd: Kbd,
  br: (props) => <chakra.div height="24px" {...props} />,
  table: Table,
  th: Th,
  td: Td,
  p: (props) => <chakra.p apply="mdx.p" {...props} />,
  ul: (props) => <chakra.ul apply="mdx.ul" {...props} />,
  ol: (props) => <chakra.ol apply="mdx.ul" {...props} />,
  li: (props) => <chakra.li pb="4px" {...props} />,
  blockquote: (props) => (
    <Alert
      role="none"
      status="success"
      variant="left-accent"
      as="blockquote"
      apply="mdx.blockquote"
      {...props}
    />
  ),
  CodeSandBox,
  a: ({ href, children, ...restProps }) => {
    function isExternal(url: string) {
      if (typeof window !== 'object') {
        return false;
      }

      const tempLink = document.createElement('a');
      tempLink.href = url;
      const result = tempLink.hostname !== window.location.hostname;
      tempLink.remove();
      return result;
    }

    if (isExternal(href)) {
      return (
        <chakra.a
          {...restProps}
          apply="mdx.a"
          href={href}
          target="_blank"
          rel="noopener noreferrer"
        >
          {children}
        </chakra.a>
      );
    }

    return (
      <Link {...restProps} to={href}>
        {children}
      </Link>
    );
  },
};
