import * as React from 'react';
import { css } from '@emotion/react';

export const IconRepeat: React.FC<{
  icon: string;
  label: string;
  amount: number;
}> = ({ icon, amount, label }) => {
  const icons = [];
  for (let i = 0; i < amount; i++) {
    icons.push(icon);
  }
  return (
    <span
      role="img"
      aria-label={label}
      css={css`
        margin-right: 0.5em;
      `}
    >
      {icons}
    </span>
  );
};
