import * as React from 'react';
import posed from 'react-pose';
import { HTMLAttributes, SyntheticEvent, useState } from 'react';
import { css } from '@emotion/react';

const Frame = posed.div({
  init: {
    applyAtEnd: { display: 'none' },
    opacity: 0,
  },
  zoom: {
    applyAtStart: { display: 'block' },
    opacity: 1,
  },
});

const transition = {
  duration: 400,
  ease: [0.08, 0.69, 0.2, 0.99],
};

const Image = posed.img({
  init: {
    position: 'static',
    width: 'auto',
    height: 'auto',
    transition,
    flip: true,
  },
  zoom: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    transition,
    flip: true,
  },
});

const ZoomImg: React.FC<
  HTMLAttributes<HTMLImageElement> & {
    width: string;
    height: string;
  }
> = ({ width = '100%', height = 'auto', ...props }) => {
  const [isZoomed, setZoomed] = useState(false);

  function zoomIn() {
    window.addEventListener('scroll', zoomOut);
    setZoomed(true);
  }

  function zoomOut() {
    window.removeEventListener('scroll', zoomOut);
    setZoomed(false);
  }

  function toggleZoom(e: SyntheticEvent) {
    e.preventDefault();

    if (isZoomed) {
      zoomOut();
      return;
    }
    zoomIn();
  }

  const pose = isZoomed ? 'zoom' : 'init';

  return (
    <div
      role="button"
      style={{ width, height }}
      onClick={toggleZoom}
      aria-label="Toggle zoom"
      aria-pressed={isZoomed}
      css={imageWrapper}
    >
      <Frame pose={pose} css={frame} />
      <Image pose={pose} {...props} />
    </div>
  );
};

const imageWrapper = css`
  img {
    cursor: zoom-in;
    display: block;
    max-width: 100%;
    margin: auto;
  }

  img.zoomed {
    cursor: zoom-out;
  }
`;

const frame = css`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: none;
  background: white;
  transform: translateZ(0);
`;

export default ZoomImg;
