import * as React from 'react';
import { useState } from 'react';

export const MyEmail = () => {
  const [email, setEmail] = useState('');

  if (typeof window !== 'undefined' && !email) {
    setEmail('hello@kolberger.eu');
  }

  return <a href={`mailto:${email}`}>{email}</a>;
};
