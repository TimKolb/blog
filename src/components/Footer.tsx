import React from 'react';
import { HStack, chakra, Flex, useColorModeValue } from '@chakra-ui/react';
import { Link } from './LinkChakraGatsby';

export const Footer: React.FC = () => (
  <Flex
    as="footer"
    wrap="wrap"
    justify="space-between"
    align="center"
    px="4"
    py="2"
    mx="auto"
    w="full"
    maxW="75rem"
    zIndex="banner"
  >
    <HStack wrap="wrap">
      <chakra.span
        fontSize="sm"
        color={useColorModeValue('gray.600', 'gray.400')}
      >
        © {new Date().getFullYear()} Tim Kolberger
      </chakra.span>
      <Link fontSize="sm" to="/datenschutz">
        Datenschutzerklärung
      </Link>
    </HStack>
  </Flex>
);
