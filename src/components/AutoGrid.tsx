import * as React from 'react';
import { Grid, GridProps, useToken } from '@chakra-ui/react';

export interface AutoGridProps extends GridProps {
  baseWidth?: string;
}

export const AutoGrid = ({ baseWidth = '16rem', ...props }: AutoGridProps) => (
  <Grid
    gap="8"
    gridTemplateColumns={`repeat(auto-fit, minmax(${useToken(
      'sizes',
      baseWidth
    )}, 1fr))`}
    {...props}
  />
);
