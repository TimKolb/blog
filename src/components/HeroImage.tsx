import * as React from 'react';
import Img, { GatsbyImageFluidProps } from 'gatsby-image';
import { Box, Text, useColorModeValue } from '@chakra-ui/react';

interface HeroImageProps extends GatsbyImageFluidProps {
  credit?: string;
}

export const HeroImage: React.FC<HeroImageProps> = ({
  credit,
  ...restProps
}) => (
  <Box>
    <Img
      alt={credit}
      {...restProps}
      draggable={false}
      style={{ minHeight: '20rem', maxHeight: 'calc(100vh - 20rem)' }}
    />
    {credit && (
      <Text
        as="span"
        fontSize="xs"
        d="block"
        textAlign="center"
        color={useColorModeValue('gray.400', 'gray.600')}
        p={2}
      >
        {credit}
      </Text>
    )}
  </Box>
);
