import * as React from 'react';
import { Link as GatsbyLink } from 'gatsby';
import {
  Link as ChakraLink,
  LinkProps as ChakraLinkProps,
  forwardRef,
  useColorModeValue,
} from '@chakra-ui/react';
import { GatsbyLinkProps } from 'gatsby-link';

export type LinkProps<TData = any> = Omit<GatsbyLinkProps<TData>, 'ref'> &
  ChakraLinkProps;
export const Link: React.FC<
  LinkProps & { highlightCurrentPage?: boolean }
> = forwardRef(({ highlightCurrentPage = true, sx, ...props }, ref) => {
  const currentPageColor = useColorModeValue('green.500', 'green.400');
  const currentPageStyle = highlightCurrentPage
    ? {
        '&[aria-current="page"]': { color: currentPageColor },
      }
    : null;

  return (
    <ChakraLink
      as={GatsbyLink}
      ref={ref}
      sx={{
        '&.chakra-button': { textDecoration: 'none' },
        ...currentPageStyle,
        ...sx,
      }}
      {...props}
    />
  );
});
