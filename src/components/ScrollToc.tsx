import * as React from 'react';
import {
  Box,
  BoxProps,
  chakra,
  useColorModeValue,
  VStack,
  Link,
} from '@chakra-ui/react';
import AnchorLink from 'react-anchor-link-smooth-scroll';
import { Toc, TocEntry } from '../templates/BlogPostTemplate';
import { motion } from 'framer-motion';

function flatten(arr: any[]): any[] {
  return arr.reduce(
    (flat, toFlatten) =>
      flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten),
    []
  );
}

function renderToc(tableOfContents: Toc) {
  function step(entry: TocEntry, level: number = 0): any[] {
    if (!entry) {
      return [];
    }

    return [
      { level, ...entry },
      ...(entry.items
        ? flatten(entry.items.map((e) => step(e, level + 1)))
        : []),
    ];
  }

  return step(tableOfContents);
}

export const ScrollToc: React.FC<
  BoxProps & {
    tableOfContents: Toc;
  }
> = ({ tableOfContents, ...boxProps }) => {
  const cardBg = useColorModeValue('blackAlpha.200', 'whiteAlpha.200');
  const linkColor = useColorModeValue('gray.800', 'gray.200');
  const list = renderToc(tableOfContents);
  const urls = list
    .map((l) => l.url && `${l.url}`.replace('#', ''))
    .filter((a) => !!a);

  return !urls.length ? null : (
    <Box {...boxProps}>
      <motion.div initial={{ translateX: -100 }} animate={{ translateX: 0 }}>
        <VStack
          as="nav"
          align="flex-start"
          p="2"
          bg={cardBg}
          borderRadius="md"
          maxH="calc(100vh - 1rem)"
          overflowY="auto"
          sx={{
            backdropFilter: 'blur(3px)',
          }}
        >
          <chakra.ol listStyleType="none">
            {list.map((entry, index) => {
              if (!entry.title) {
                return;
              }

              return (
                <li key={`${entry.title}${index}`}>
                  {entry.url && entry.title && (
                    <Link
                      as={AnchorLink}
                      transition="color 150ms ease-in"
                      color={linkColor}
                      _hover={{ color: 'green.500' }}
                      _focus={{ color: 'green.500' }}
                      href={entry.url}
                      aria-current="page"
                      display="inline-block"
                      fontSize={`${(6 - entry.level + 10) / 16}rem`}
                      pl={`${entry.level * 0.5}em`}
                    >
                      {entry.title}
                    </Link>
                  )}
                </li>
              );
            })}
          </chakra.ol>
        </VStack>
      </motion.div>
    </Box>
  );
};
