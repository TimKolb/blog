import * as React from 'react';
import { HTMLAttributes, useMemo } from 'react';
import md5 from 'blueimp-md5';
import { Img } from '@chakra-ui/react';

export const Gravatar: React.FC<
  { email: string } & HTMLAttributes<HTMLImageElement>
> = ({ email, ...restProps }) => {
  const gravatarUrl = useMemo(
    () => `https://www.gravatar.com/avatar/${md5(email)}`,
    [email]
  );
  return (
    <Img
      src={gravatarUrl}
      alt={email}
      boxSize="10"
      borderRadius="full"
      {...restProps}
    />
  );
};
