import React from 'react';
import { Gravatar } from './Gravatar';
import {
  Button,
  ButtonGroup,
  Flex,
  ButtonProps,
  useColorModeValue,
  Box,
} from '@chakra-ui/react';
import { Link, LinkProps } from './LinkChakraGatsby';
import { DarkModeToggle } from './DarkModeToggle';

type HeaderProps = {
  siteTitle?: string;
};

export const Header: React.FC<HeaderProps> = () => (
  <Box
    as="header"
    w="full"
    bg={useColorModeValue('whiteAlpha.600', 'blackAlpha.600')}
    boxShadow="md"
    zIndex="banner"
    sx={{
      backdropFilter: 'blur(0.5rem)',
    }}
  >
    <Flex
      alignItems="center"
      justify="space-between"
      mx="auto"
      p="4"
      maxW="75rem"
    >
      <Link to="/" d="flex" alignItems="center">
        <Gravatar email="tim@kolberger.eu" />
      </Link>
      <Menu />
    </Flex>
  </Box>
);

const MenuLink = (props: ButtonProps & LinkProps) => (
  <Button
    as={Link}
    colorScheme={useColorModeValue('black', 'white')}
    {...props}
  />
);

export const Menu = () => {
  return (
    <ButtonGroup as="nav" variant="ghost">
      <MenuLink to="/blog">Blog</MenuLink>
      <MenuLink to="/contact">Contact</MenuLink>
      <DarkModeToggle />
    </ButtonGroup>
  );
};
