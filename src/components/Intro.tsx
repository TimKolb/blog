import * as React from 'react';
import {
  Flex,
  Box,
  VStack,
  Heading,
  Text,
  Button,
  DarkMode,
  chakra,
} from '@chakra-ui/react';
import { EyeCatcher } from './EyeCatcher';
import { Link } from './LinkChakraGatsby';
import { PageTransition } from './PageTransition';

export const Intro: React.FC = () => {
  return (
    <Flex
      flex="1 0 auto"
      maxH="calc(100vh - 4rem)"
      direction="column"
      justify="flex-end"
      align="center"
      bg="gray.800"
    >
      <Box position="absolute" top="0" left="0" w="full" h="full" zIndex="0">
        <EyeCatcher />
      </Box>
      <PageTransition flex="0">
        <VStack
          zIndex="docked"
          p="8"
          color="white"
          bg="whiteAlpha.200"
          borderRadius="md"
          spacing="4"
          sx={{
            backdropFilter: 'blur(3px)',
          }}
          mt="4"
          mb="10vh"
          boxShadow="lg"
        >
          <Heading
            as="h1"
            size="2xl"
            bgGradient="linear(to-l, green.400, purple.400)"
            bgClip="text"
          >
            <span>Hi</span>{' '}
            <chakra.span
              role="img"
              aria-label="Waving Hand"
              bgGradient="linear(to-l, pink.600, purple.600)"
              bgClip="text"
            >
              👋
            </chakra.span>{' '}
            <span>I am Tim&nbsp;Kolberger</span>
          </Heading>
          <Text>I blog about [JavaScript, React, DevOps, ...more]</Text>
          <DarkMode>
            <Button as={Link} to="/blog" sx={{ pointerEvents: 'all' }}>
              Start reading
            </Button>
          </DarkMode>
        </VStack>
      </PageTransition>
    </Flex>
  );
};
