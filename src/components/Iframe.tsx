import * as React from 'react';
import { HTMLProps } from 'react';
import { chakra } from '@chakra-ui/react';

export const Iframe: React.FC<HTMLProps<HTMLIFrameElement>> = (props) => (
  <chakra.div
    position="relative"
    paddingBottom="56.25%" /* ratio 16x9 */
    height="0"
    overflow="hidden"
    borderRadius="md"
    boxShadow="md"
    mb="6"
  >
    <chakra.div
      {...props}
      as="iframe"
      maxWidth="full"
      position="absolute"
      top="0"
      left="0"
      width="full"
      height="full"
      border="none"
    />
  </chakra.div>
);
