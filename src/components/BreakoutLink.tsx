import * as React from 'react';
import { Link } from 'gatsby';
import {
  Link as ChakraLink,
  LinkProps as ChakraLinkProps,
} from '@chakra-ui/react';
import { GatsbyLinkProps } from 'gatsby-link';

const breakoutLinkStyle = {
  position: 'static',
  display: 'block',
  _hover: {
    textDecoration: 'none',
  },
  '&::before': {
    content: "''",
    cursor: 'inherit',
    display: 'block',
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
  },
};

export type BreakoutLinkProps<TData> = Omit<GatsbyLinkProps<TData>, 'ref'> &
  ChakraLinkProps;
export const BreakoutLink: React.FC<BreakoutLinkProps<any>> = (props) => (
  <ChakraLink as={Link} sx={breakoutLinkStyle} {...props} />
);
