import React from 'react';
import { IconButton, useColorMode } from '@chakra-ui/react';
import { FaSun, FaMoon } from 'react-icons/fa';

export type DarkModeToggleProps = {};
export const DarkModeToggle: React.FC<DarkModeToggleProps> = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  const icon = colorMode === 'light' ? <FaMoon /> : <FaSun />;

  return (
    <IconButton
      onClick={toggleColorMode}
      aria-label="Toggle theme"
      icon={icon}
      variant="ghost"
    />
  );
};
