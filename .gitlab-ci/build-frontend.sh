#!/usr/bin/env sh

set -ex

TAGS="-t $TAGGED_IMAGE_TAG_FRONTEND"

if [ "${CI_COMMIT_REF_SLUG}" = "master" ]; then
  TAGS="${TAGS} -t ${IMAGE_TAG_FRONTEND}:latest"
fi

docker pull $TAGGED_IMAGE_TAG_FRONTEND || true
docker pull $IMAGE_TAG_FRONTEND:develop || true

docker build \
    ${TAGS} \
    -f .docker/frontend/Dockerfile \
    --build-arg HOST_URL \
    .

docker push $TAGGED_IMAGE_TAG_FRONTEND
