/**
 * Implement Gatsby's SSR (Server Side Rendering) APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/ssr-apis/
 */

import React, { StrictMode } from 'react';
import { MDXProvider } from '@mdx-js/react';
import { ChakraProvider } from '@chakra-ui/react';
import theme from './src/@chakra-ui/gatsby-plugin/theme';
import { components } from './src/components/mdxComponents';
import './src/styles/global.css';

export const wrapRootElement = ({ element }) => (
  <StrictMode>
    <ChakraProvider theme={theme}>
      <MDXProvider components={components}>{element}</MDXProvider>
    </ChakraProvider>
  </StrictMode>
);
