/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

import React, { StrictMode } from 'react';
import { MDXProvider } from '@mdx-js/react';
import { components } from './src/components/mdxComponents';
import { ChakraProvider } from '@chakra-ui/react';
import theme from './src/@chakra-ui/gatsby-plugin/theme';
import 'react-medium-image-zoom/dist/styles.css';
import './src/styles/global.css';

export const wrapRootElement = ({ element }) => {
  return (
    <StrictMode>
      <ChakraProvider theme={theme}>
        <MDXProvider components={components}>{element}</MDXProvider>
      </ChakraProvider>
    </StrictMode>
  );
};
