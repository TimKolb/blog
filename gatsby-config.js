const config = {
  siteMetadata: {
    title: `Tim Kolberger`,
    description: `Personal Blog of Tim Kolberger`,
    author: `Tim Kolberger`,
    social: {
      twitter: '@TimKolberger',
    },
    siteUrl: process.env.HOST_URL
      ? 'https://' + process.env.HOST_URL
      : 'http://localhost:8000',
  },
  plugins: [
    {
      resolve: `gatsby-plugin-feed`,
      options: {
        query: `
          {
            site {
              siteMetadata {
                title
                description
                siteUrl
                site_url: siteUrl
              }
            }
          }
        `,
        feeds: [
          {
            serialize: ({ query: { site, allMdx } }) => {
              return allMdx.edges.map((edge) => {
                return Object.assign({}, edge.node.frontmatter, {
                  description: edge.node.frontmatter.spoiler,
                  date: edge.node.frontmatter.date,
                  url: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  guid: site.siteMetadata.siteUrl + edge.node.fields.slug,
                  custom_elements: [{ 'content:encoded': edge.node.html }],
                });
              });
            },
            query: `
              {
                allMdx(
                  filter: {
                    fileAbsolutePath: { regex: "content/blog/i" }
                    frontmatter: { isDraft: { ne: true }, title: { ne: "" } }
                  }
                  sort: { fields: [frontmatter___date], order: DESC }
                ) {
                  edges {
                    node {
                      id
                      fields {
                        slug
                      }
                      frontmatter {
                        date
                        title
                        spoiler
                      }
                    }
                  }
                }
              }
            `,
            output: '/rss.xml',
            title: 'Tim Kolberger | Blog - RSS',
          },
        ],
      },
    },
    {
      resolve: 'gatsby-plugin-mdx',
      options: {
        extensions: ['.mdx', '.md'],
        defaultLayouts: {
          pages: require.resolve('./src/layout/PageLayout.tsx'),
        },
        gatsbyRemarkPlugins: [
          {
            resolve: 'gatsby-remark-images',
            options: { linkImagesToOriginal: false },
          },
          {
            resolve: 'gatsby-remark-prismjs',
            options: {
              inlineCodeMarker: '÷',
            },
          },
          { resolve: 'gatsby-remark-smartypants' },
          { resolve: 'gatsby-remark-autolink-headers' },
        ],
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/content/blog`,
        name: 'blog',
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          'Fira Code',
          'Roboto Slab',
          'Mukta Vaani:400,500,700',
          'Alegreya Sans:400,500,700',
          'Catamaran:200,300,400,500,700',
        ],
        display: 'swap',
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Tim Kolberger | Blog`,
        short_name: `TK Blog`,
        start_url: `/`,
        background_color: `#b4d455`,
        theme_color: `#b4d455`,
        display: `minimal-ui`,
        icon: `src/images/tim-kolberger-logo.png`,
      },
    },
    'gatsby-plugin-sitemap',
    'gatsby-plugin-catch-links',
    'gatsby-plugin-typescript',
  ],
};

if (process.env.PATH_PREFIX) {
  config.pathPrefix = process.env.PATH_PREFIX;
}

module.exports = config;
